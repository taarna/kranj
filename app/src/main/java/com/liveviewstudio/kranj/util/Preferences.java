package com.liveviewstudio.kranj.util;

import android.content.SharedPreferences;

/**
 * Handler class for application shared preference.
 *
 */
public class Preferences {

    private static Preferences sKranjPreferences;

    private final SharedPreferences mSharedPreferences;

    /**
     * Preference type.
     *
     * Possible values:<br>
     * APP_VERSION - current version of the app<br>
     * LANGUAGE - selected language
     */
    public enum PreferenceType {
        APP_VERSION,
        IS_FIRST_START,
        LANGUAGE
    }

    private Preferences(SharedPreferences sharedPreferences) {
        this.mSharedPreferences = sharedPreferences;
    }

    /**
     * Initialize <code>Preference</code> instance.
     *
     * @param sharedPreferences SharedPreferences
     */
    public static void init(SharedPreferences sharedPreferences) {
        synchronized (Preferences.class) {
            if (sKranjPreferences == null) {
                sKranjPreferences = new Preferences(sharedPreferences);
            }
        }
    }

    /**
     * Gets <code>Preference</code> instance. Returns one copy per application lifecycle.
     *
     * @return Preferences instance
     */
    public static Preferences getInstance() {
        if (sKranjPreferences == null) {
            throw new IllegalStateException("Preference not initialized. Call init() first");
        }
        return sKranjPreferences;
    }

    public SharedPreferences getSharedPreferences() {
        return mSharedPreferences;
    }

    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mSharedPreferences.registerOnSharedPreferenceChangeListener(listener);
    }

    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mSharedPreferences.unregisterOnSharedPreferenceChangeListener(listener);
    }

    @SuppressWarnings("unchecked")
    public <T> T getPreference(PreferenceType preferenceType) {
        T genericValue = null;
        switch (preferenceType) {
            case APP_VERSION:
                Float appVersion = mSharedPreferences.getFloat(preferenceType.name(), 1.0f);
                genericValue = (T)appVersion;
                break;
            case LANGUAGE:
                genericValue = (T)mSharedPreferences.getString(preferenceType.name(), null);
                break;
            case IS_FIRST_START:
                Boolean isFirstStart = mSharedPreferences.getBoolean(preferenceType.name(), true);
                genericValue = (T)isFirstStart;
                break;
        }
        return genericValue;
    }

    public void setPreference(PreferenceType preferenceType, Object value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        switch (preferenceType) {
            case IS_FIRST_START:
                editor.putBoolean(preferenceType.name(), (Boolean)value);
                break;
            case APP_VERSION:
                editor.putFloat(preferenceType.name(), (Float) value);
                break;
            case LANGUAGE:
                editor.putString(preferenceType.name(), (String) value);
                break;
        }
        editor.commit();
    }

}
