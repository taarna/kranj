package com.liveviewstudio.kranj.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.liveviewstudio.kranj.R;
import com.liveviewstudio.kranj.adapter.InstructionsPagerAdapter;
import com.viewpagerindicator.IconPageIndicator;

public class InstructionsActivity extends Activity {

    private ViewPager mViewPagerInstructions;
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);

        // Instantiate a ViewPager and a PagerAdapter.
        mViewPagerInstructions = (ViewPager)findViewById(R.id.viewPagerInstructions);
        mPagerAdapter = new InstructionsPagerAdapter(this);
        mViewPagerInstructions.setAdapter(mPagerAdapter);

        IconPageIndicator mPageIndicator = (IconPageIndicator)findViewById(R.id.viewPagerIndicator);
        mPageIndicator.setViewPager(mViewPagerInstructions);
        mPageIndicator.setCurrentItem(0);

        // Listener -> Opening Map activity when we reach the last page
        ViewPager.OnPageChangeListener pagerListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //not used
            }
            @Override
            public void onPageSelected(int position) {
                //not used
            }
            @Override
            public void onPageScrollStateChanged(int state) {
                int lastIdx = mPagerAdapter.getCount() - 1;
                int curItem = mViewPagerInstructions.getCurrentItem();

                if (curItem==lastIdx && state==1) {
                    openMapActivity();
                }
            }
        };
        mPageIndicator.setOnPageChangeListener(pagerListener);
    }

    private void openMapActivity() {
        Intent intent = new Intent(InstructionsActivity.this, MapActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

}
