package com.liveviewstudio.kranj.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.liveviewstudio.kranj.BuildConfig;
import com.liveviewstudio.kranj.R;
import com.liveviewstudio.kranj.adapter.DatabaseAdapter;
import com.liveviewstudio.kranj.dialog.CustomDialog;
import com.liveviewstudio.kranj.layout.CustomMapFrame;
import com.liveviewstudio.kranj.model.Hotspot;
import com.liveviewstudio.kranj.model.Poi;
import com.liveviewstudio.kranj.util.GeneralUtil;
import com.metaio.sdk.MetaioDebug;
import com.metaio.tools.io.AssetsManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapActivity extends FragmentActivity
        implements OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener,
        CustomMapFrame.OnMapDragListener,
        View.OnClickListener,
        Animation.AnimationListener,
        CustomDialog.OnCustomDialogClickListener {

    private final LatLng INITIAL_MAP_LOCATION = new LatLng(46.239480, 14.355868);

    private GoogleMap mMap;

    private CustomMapFrame mMapFrame;

    /** We need to store hotspots just so we wouldn't have to select additional stuff from the DB
     * every time the user taps one of the markers. */
    protected static Map<Marker, Hotspot> mMarkers;

    private Marker mCurrentlySelectedMarker;

    private MapAsyncTask mMapAsyncTask;

    private ImageView mImageView;
    private Button mButtonA;
    private Button mButtonD;

    private FrameLayout.LayoutParams mImageViewLayout=new FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT);
    private FrameLayout.LayoutParams mButtonALayout=new FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT);
    private FrameLayout.LayoutParams mButtonDLayout=new FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT);

    private Animation mImageViewAnimation;
    private Animation mButtonARAnimation;
    private Animation mButtonDetailsAnimation;
    private OvershootInterpolator mOvershootInterpolator=new OvershootInterpolator();

    protected static ArrayList<Poi> mPois;

    /**
     * Task that will extract all the assets
     */
    AssetsExtracter mTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(this);

        /** CustomMapFrame is a wrapper around the map and its only purpose is to handle
         * gestures. This is needed because we need to remove fake InfoWindow if the user
         * taps somewhere or if he/she starts swiping/scrolling. */
        mMapFrame = (CustomMapFrame)findViewById(R.id.map_custom_frame);
        mMapFrame.setOnDragListener(this);

        Button buttonInstructions = (Button)findViewById(R.id.btn_instructions);
        buttonInstructions.setOnClickListener(this);

        mMarkers = new HashMap<Marker, Hotspot>();

        //dynamic widget creation
        mImageView=new ImageView(this);
        mImageView.setId(R.id.markerBigImage);
        mImageView.setOnClickListener(this);
        mButtonA=new Button(this);
        mButtonA.setBackgroundResource(R.drawable.btn_ar);
        mButtonA.setId(R.id.buttonAR);
        mButtonA.setOnClickListener(this);
        mButtonA.setVisibility(View.INVISIBLE);
        mButtonD=new Button(this);
        mButtonD.setBackgroundResource(R.drawable.btn_details);
        mButtonD.setId(R.id.buttonDetails);
        mButtonD.setOnClickListener(this);
        mButtonD.setVisibility(View.INVISIBLE);

        mImageViewAnimation=AnimationUtils.loadAnimation(this, R.anim.pop_in);
        mImageViewAnimation.setFillAfter(true);
        mImageViewAnimation.setInterpolator(mOvershootInterpolator);
        mImageViewAnimation.setAnimationListener(this);

        mButtonARAnimation=AnimationUtils.loadAnimation(this, R.anim.pop_in);
        mButtonARAnimation.setFillAfter(true);
        mButtonARAnimation.setInterpolator(mOvershootInterpolator);
        mButtonARAnimation.setAnimationListener(this);

        mButtonDetailsAnimation=AnimationUtils.loadAnimation(this, R.anim.pop_in);
        mButtonDetailsAnimation.setFillAfter(true);
        mButtonDetailsAnimation.setInterpolator(mOvershootInterpolator);
        mButtonDetailsAnimation.setAnimationListener(this);

        // extract all the assets
        mTask = new AssetsExtracter();
        mTask.execute(0);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                if (!GeneralUtil.isConnectedToNetwork(MapActivity.this)) {
                    showLocationWarningDialog();
                }
            }
        }, 2500);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!GeneralUtil.isConnectedToNetwork(MapActivity.this)) {

        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("MAP", "READY");
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        mMap.setOnMarkerClickListener(this);

        /** Move and zoom the camera. */
        mMap.moveCamera(CameraUpdateFactory.newLatLng(INITIAL_MAP_LOCATION));

        CameraUpdate cameraUpdate =
                CameraUpdateFactory.newLatLngZoom(INITIAL_MAP_LOCATION, 18);
        mMap.animateCamera(cameraUpdate, 1500, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                /** These are the only two gestures we can recognize using mMap object. Everything else
                 * should be handled using the class CustomMapFrame. */
                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        mCurrentlySelectedMarker = null;
                        hideInfoWindow();
                    }
                });
                mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override
                    public void onMapLongClick(LatLng latLng) {
                        mCurrentlySelectedMarker = null;
                        hideInfoWindow();
                    }
                });

                mMapAsyncTask = new MapAsyncTask(MapActivity.this);
                mMapAsyncTask.execute();
            }

            @Override
            public void onCancel() {
                //not used
            }
        });
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {

        hideInfoWindow();
        mCurrentlySelectedMarker = marker;

        CameraUpdate cameraUpdate =
                CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 16);
        mMap.animateCamera(cameraUpdate, 500, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                showInfoWindow(marker);
            }

            @Override
            public void onCancel() {
                //not used
            }
        });

        return true;
    }

    @Override
    public void onDrag() {
        mCurrentlySelectedMarker = null;
        hideInfoWindow();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.buttonAR) {
            openARActivity(mCurrentlySelectedMarker);
        } else if (v.getId() == R.id.buttonRadar) {
            Hotspot hotspot = mMarkers.get(mCurrentlySelectedMarker);
            PoiAsyncTask poiAsyncTask = new PoiAsyncTask(MapActivity.this, hotspot.getId());
            poiAsyncTask.execute();
        } else if (v.getId() == R.id.buttonDetails) {
            openDetailsActivity(mCurrentlySelectedMarker);
        } else if (v.getId() == R.id.btn_instructions) {
            openInstructionsActivity();
        }
    }

    private void showInfoWindow(final Marker marker) {

        //Find the position of the marker
        Projection projection = mMap.getProjection();
        LatLng markerLocation = marker.getPosition();
        Point markerScreenPosition = projection.toScreenLocation(markerLocation);

        //Set the correct image
        Hotspot hotspot = mMarkers.get(marker);
        String imageName = hotspot.getInfoImageName();
        Drawable image = GeneralUtil.getDrawable(MapActivity.this, imageName);
        mImageView.setImageDrawable(image);

        //Calculate the position of big image
        Display display = getWindowManager().getDefaultDisplay();
        Point screenSize = new Point();
        display.getSize(screenSize);

        final int screenWidth = screenSize.x;
        final int screenHeight = screenSize.y;

        final int xDiff = screenWidth/2 - markerScreenPosition.x;
        final int yDiff = screenHeight/2 - markerScreenPosition.y;

        final int imageWidthHeight = mImageView.getDrawable().getIntrinsicWidth();

        final int imageX = screenSize.x/2 -  imageWidthHeight/2 - xDiff;
        final int imageY = screenSize.y/2 - imageWidthHeight/2 - yDiff;

        mImageViewLayout.setMargins(imageX, imageY, 0, 0);

        mMapFrame.addView(mImageView, mImageViewLayout);

        mImageView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mImageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        addButtonsRelative((int) mImageView.getX(), (int) mImageView.getY(),
                                mImageView.getWidth(), mImageView.getHeight());

                    }
                });
        mImageView.startAnimation(mImageViewAnimation);
    }

    private void hideInfoWindow() {
        mMapFrame.removeView(mImageView);
        mMapFrame.removeView(mButtonA);
        mMapFrame.removeView(mButtonD);
    }

    private void addButtonsRelative(int x, int y, int width, int height) {

        final int buttonWidthHeight = mButtonA.getBackground().getIntrinsicWidth();

        final double cosZero = Math.cos(Math.toRadians(0));
        final double sinZero = Math.sin(Math.toRadians(0));

        final double cosFourtyFive = Math.cos(Math.toRadians(45));
        final double sinFourtyFive = Math.sin(Math.toRadians(45));

        int marginX = (int)(width/2 * cosZero + x + width/2 + buttonWidthHeight/6);
        int marginY = (int)(height/2 * sinZero + y + height/2);

        //Button AR
        mButtonALayout.setMargins(marginX, marginY, 0, 0);
        mMapFrame.addView(mButtonA, mButtonALayout);

        //Button Details
        marginX = (int)(width/2 * cosFourtyFive + x + width/2);
        marginY = (int)(height/2 * sinFourtyFive + y + height/2);
        mButtonDLayout.setMargins(marginX, marginY, 0, 0);
        mMapFrame.addView(mButtonD, mButtonDLayout);
    }

    /** Opens Instructions activity. */
    private void openInstructionsActivity() {
        Intent intent = new Intent(MapActivity.this, InstructionsActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    /** Opens Details activity. */
    private void openDetailsActivity(Marker marker) {
        Intent intent = new Intent(MapActivity.this, DetailsActivity.class);

        if (marker != null) {
            Hotspot hotspot = mMarkers.get(marker);
            intent.putExtra(getResources().getString(R.string.bundleKeyHotspot), hotspot);
        }

        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    private void openRadarActivity() {
        Intent intent = new Intent(MapActivity.this, RadarActivity.class);

        if (mCurrentlySelectedMarker!=null && mPois.size()>0) {
            Hotspot hotspot = mMarkers.get(mCurrentlySelectedMarker);
            intent.putExtra(getResources().getString(R.string.bundleKeyHotspot), hotspot);
        }

        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    /** Opens AR activity. */
    private void openARActivity(Marker marker) {
        Intent intent = new Intent(MapActivity.this, ArActivity.class);

        if (marker != null) {
            Hotspot hotspot = mMarkers.get(marker);
            intent.putExtra(getResources().getString(R.string.bundleKeyHotspot), hotspot);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        //not used
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == mImageViewAnimation) {
            mButtonA.startAnimation(mButtonARAnimation);
        } else if (animation == mButtonARAnimation) {
            mButtonD.startAnimation(mButtonDetailsAnimation);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        //not used
    }

    private void showLocationWarningDialog() {
        final CustomDialog customDialog = new CustomDialog(MapActivity.this,
                getString(R.string.dialog_button_no), getString(R.string.dialog_button_yes),
                getString(R.string.dialog_title_map_warning),
                getString(R.string.dialog_text_map_warning));
        customDialog.setOnCustomDialogClickListener(this);

        customDialog.show();
    }

    @Override
    public void onRightClick() {
        Intent callGPSSettingIntent = new Intent(Settings.ACTION_SETTINGS);
        startActivityForResult(callGPSSettingIntent, 0);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    private class MapAsyncTask extends AsyncTask<Void, Void, List<Hotspot>> {
        private Context mContext;

        public MapAsyncTask(Context context) {
            mContext = context;
        }

        @Override
        protected List<Hotspot> doInBackground(Void... params) {

            DatabaseAdapter mDbAdapter = new DatabaseAdapter(mContext);

            mDbAdapter.createDatabase();
            mDbAdapter.open();

            List<Hotspot> hotspotList = mDbAdapter.getHotspotList();

            mDbAdapter.close();

            return hotspotList;
        }

        @Override
        protected void onPostExecute(List<Hotspot> msg) {
            for (Hotspot hotspot: msg) {
                Bitmap pinImage = createPinImage(hotspot.getPinImageName());
                final Marker marker = mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(hotspot.getLatitude(), hotspot.getLongitude()))
                        .title(hotspot.getName())
                        .icon(BitmapDescriptorFactory.fromBitmap(pinImage))
                        .visible(true));
                mMarkers.put(marker, hotspot);
            }
        }

        private Bitmap createPinImage(String imageName) {

            //Inflate pin layout
            LayoutInflater inflater = ((LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE));
            RelativeLayout pinLayout = (RelativeLayout)inflater.inflate(R.layout.layout_pin, null);

            //Find ImageView (one that's displayed in front of pin background)
            ImageView pinImageView = (ImageView)pinLayout.findViewById(R.id.img_pin_image);

            //Find the id of the image
            final int resourceId = GeneralUtil.getResourceId(mContext, imageName, "drawable");

            if (resourceId!=0) {
                Drawable drawable = GeneralUtil.getDrawable(mContext, resourceId);

                if (drawable!=null) {
                    pinImageView.setImageDrawable(drawable);
                }
            }

            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            pinLayout.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
            pinLayout.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
            pinLayout.buildDrawingCache();
            Bitmap bitmap = Bitmap.createBitmap(pinLayout.getMeasuredWidth(),
                    pinLayout.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(bitmap);
            pinLayout.draw(canvas);

            return bitmap;
        }

    }

    /**
     * This task extracts all the assets to an external or internal location
     * to make them accessible to Metaio SDK
     */
    private class AssetsExtracter extends AsyncTask<Integer, Integer, Boolean> {

        @Override
        protected void onPreExecute() {
            //not used
        }

        @Override
        protected Boolean doInBackground(Integer... params)
        {
            try {
                // Extract all assets except Menu. Overwrite existing files for debug build only.
                final String[] ignoreList = {"kranj.db", "Menu", "webkit", "sounds", "images", "webkitsec"};
                AssetsManager.extractAllAssets(getApplicationContext(), "", ignoreList, BuildConfig.DEBUG);
            } catch (IOException e) {
                MetaioDebug.printStackTrace(Log.ERROR, e);
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (!result) {
                MetaioDebug.log(Log.ERROR, "Error extracting assets, closing the application...");
                finish();
            }
        }
    }

    private class PoiAsyncTask extends AsyncTask<Void, Void, Void> {
        private Context mContext;
        private int mHotspotId;

        public PoiAsyncTask(Context context, int hotspotId) {
            mContext = context;
            mHotspotId = hotspotId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            DatabaseAdapter mDbAdapter = new DatabaseAdapter(mContext);

            if (mPois!=null) {
                mPois.clear();
            }

            mDbAdapter.open();
            mPois = (ArrayList<Poi>)mDbAdapter.getPoiListForHotspot(mHotspotId);
            mDbAdapter.close();

            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            openRadarActivity();
        }
    }

}
