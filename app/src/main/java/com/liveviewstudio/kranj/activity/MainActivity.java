package com.liveviewstudio.kranj.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.liveviewstudio.kranj.R;
import com.liveviewstudio.kranj.util.Preferences;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* Language screen should only be shown on first app start. However, if the user
        * didn't select the language on first app start, we should display the language screen
        * until he/she picks one. */
        Boolean isFirstAppStart = isFirstAppStart();
        Boolean isLanguageSet = isLanguageSet();

        if (isFirstAppStart || !isLanguageSet) {
            setFirstAppStartFlag(false);
            //Open Instructions
            openLanguageActivity();
        } else {
            //Open map activity
            openMapActivity();
        }
    }

    /** Checks if the app was started for the first time.
     * @return true if it's the first start, false if it's not. */
    private Boolean isFirstAppStart() {
        return Preferences.getInstance().getPreference(Preferences.PreferenceType.IS_FIRST_START);
    }

    /** Checks if the language selected.
     * @return true if the language is already selected, false if it's not. */
    private Boolean isLanguageSet() {
        String language = Preferences.getInstance().
                getPreference(Preferences.PreferenceType.LANGUAGE);
        return (language != null) && (language.compareTo("") != 0);
    }

    /** Changes the value of the flag that's used to determine if it app was started for the first time.
     * @param firstTimeFlag true if the value should be set to true, otherwise false. */
    private void setFirstAppStartFlag(Boolean firstTimeFlag) {
        Preferences.getInstance().
                setPreference(Preferences.PreferenceType.IS_FIRST_START, firstTimeFlag);
    }

    /** Opens language activity. Finishes the current activity. */
    private void openLanguageActivity() {
        Intent intent = new Intent(MainActivity.this, LanguageActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    /** Opens hotspot map. Finishes the current activity. */
    private void openMapActivity() {
        Intent intent = new Intent(MainActivity.this, MapActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

}
