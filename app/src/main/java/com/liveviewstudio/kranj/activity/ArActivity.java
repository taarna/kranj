package com.liveviewstudio.kranj.activity;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.liveviewstudio.kranj.R;
import com.liveviewstudio.kranj.model.Hotspot;
import com.liveviewstudio.kranj.util.Preferences;
import com.metaio.sdk.ARViewActivity;
import com.metaio.sdk.MetaioDebug;
import com.metaio.sdk.jni.IGeometry;
import com.metaio.sdk.jni.IMetaioSDKCallback;
import com.metaio.sdk.jni.Rotation;
import com.metaio.sdk.jni.TrackingValues;
import com.metaio.sdk.jni.TrackingValuesVector;
import com.metaio.sdk.jni.Vector3d;
import com.metaio.tools.io.AssetsManager;

import java.io.File;
import java.io.IOException;

public class ArActivity extends ARViewActivity {

    private IGeometry mMetaioPlane;

    private MetaioSDKCallbackHandler mCallbackHandler;

    private Hotspot mHotspot;

    private boolean isAnimation=false;

    private MediaPlayer mMediaPlayer;
    private boolean isAudioReady=false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mMetaioPlane = null;

        mCallbackHandler = new MetaioSDKCallbackHandler();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mHotspot = extras.getParcelable(getResources().getString(R.string.bundleKeyHotspot));

            if (mHotspot.getId()==1 || mHotspot.getId()==2 || mHotspot.getId()==5) {
                isAnimation = true;
                loadAudioFile();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mCallbackHandler.delete();
        mCallbackHandler = null;
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (isAnimation) {
            mMetaioPlane.stopAnimation();
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (isAnimation) {
            mMetaioPlane.stopAnimation();
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.stop();
            }
        }
    }

    @Override
    protected int getGUILayout() {
        return R.layout.activity_ar;
    }

    @Override
    protected void loadContents() {
        try {
            //Load desired tracking data for planar marker tracking
            final File trackingConfigFile =
                    AssetsManager.getAssetPathAsFile(getApplicationContext(),
                            "ar/" + mHotspot.getTrackingName());
            final boolean isTrackingSet = metaioSDK.setTrackingConfiguration(trackingConfigFile);

            //Model name
            final File modelName = AssetsManager.
                    getAssetPathAsFile(getApplicationContext(), "ar/" + mHotspot.getModelName());

            if (isTrackingSet && (modelName != null)) {
                if (isAnimation) {
                    mMetaioPlane = metaioSDK.createGeometry(modelName);
                } else {
                    mMetaioPlane = metaioSDK.createGeometryFromMovie(modelName, true);
                }
                if (mMetaioPlane != null) {
                    setMetaioPlaneParams();
                    setActiveModel();
                } else {
                    MetaioDebug.log(Log.ERROR, "Error loading geometry: " + modelName);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onGeometryTouched(IGeometry geometry) {
        //not used
    }

    @Override
    protected IMetaioSDKCallback getMetaioSDKCallbackHandler() {
        return mCallbackHandler;
    }

    private void setActiveModel() {
        // Start or pause movie according to tracking state
        mCallbackHandler.onTrackingEvent(metaioSDK.getTrackingValues());
    }

    private void setMetaioPlaneParams() {
        //TODO These should probably be in the DB, not here.
        switch(mHotspot.getId()) {
            case 1://House
                mMetaioPlane.setTranslation(new Vector3d(-61, -800, 400));
                mMetaioPlane.setRotation(new Rotation(0f, 0f, 0f));
                mMetaioPlane.setScale(60f);
                break;
            case 2://Pillory
                mMetaioPlane.setTranslation(new Vector3d(0, -40, 2));
                mMetaioPlane.setRotation(new Rotation(1.54f, 0f, 0f));
                mMetaioPlane.setScale(40f);
                break;
            case 3://Church
                mMetaioPlane.setTranslation(new Vector3d(0, 0, 0));
                mMetaioPlane.setRotation(new Rotation(new Vector3d(0.0f, 0.0f, -1.54f)));
                mMetaioPlane.setScale(10f);
                break;
            case 4://Coat of Arms
                mMetaioPlane.setTranslation(new Vector3d(0, 0, 0));
                mMetaioPlane.setRotation(new Rotation(new Vector3d(0f, 0f, 3.14f)));
                mMetaioPlane.setScale(10f);
                break;
            case 5://Catalog
                mMetaioPlane.setTranslation(new Vector3d(0, -39, 0));
                mMetaioPlane.setRotation(new Rotation(new Vector3d(1.54f, 0f, 0f)));
                mMetaioPlane.setScale(40f);
                break;
            case 6://Pavsler
                mMetaioPlane.setTranslation(new Vector3d(20, 100, 0));
                mMetaioPlane.setRotation(new Rotation(new Vector3d(0f, 0f, -1.59f)));
                mMetaioPlane.setScale(10f);
                break;
        }
    }

    private void openDetailsActivity() {
        if (!isAnimation) {
            mMetaioPlane.pauseMovieTexture();
        } else {
            mMetaioPlane.stopAnimation();
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            }
        }

        Intent intent = new Intent(ArActivity.this, DetailsActivity.class);
        intent.putExtra(getResources().getString(R.string.bundleKeyHotspot), mHotspot);

        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    private void loadAudioFile() {
        String language =
                Preferences.getInstance().getPreference(Preferences.PreferenceType.LANGUAGE);
        String fileName = "ar/" + mHotspot.getAudioFileName() + "_" + language + ".mp3";

        if (mMediaPlayer == null) {
            AssetFileDescriptor afd = null;
            try {
                afd = getAssets().openFd(fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }

            mMediaPlayer = new MediaPlayer();
            try {
                mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            } catch (IOException e) {
                e.printStackTrace();
            }

            mMediaPlayer.prepareAsync();

            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer player) {
                    isAudioReady = true;
                }
            });
            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
                @Override
                public void onCompletion(MediaPlayer player) {
                    mMetaioPlane.setVisible(false);
                }
            });
        }
    }

    final private class MetaioSDKCallbackHandler extends IMetaioSDKCallback {
        @Override
        public void onSDKReady() {
            // show GUI after SDK is ready
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mGUIView.setVisibility(View.VISIBLE);

                    Button btnDetails = (Button)findViewById(R.id.btn_details_ar);
                    btnDetails.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            openDetailsActivity();
                        }
                    });
                }
            });
        }
        @Override
        public void onTrackingEvent(TrackingValuesVector trackingValues) {
            super.onTrackingEvent(trackingValues);

            int cosID = trackingValues.get(0).getCoordinateSystemID();
            mMetaioPlane.setCoordinateSystemID(cosID);

            if (cosID <= 5) {
                if (mMetaioPlane != null) {
                    if (trackingValues.isEmpty() || !trackingValues.get(0).isTrackingState()) {
                        if (!isAnimation) {
                            mMetaioPlane.pauseMovieTexture();
                        } else {
                            mMetaioPlane.stopAnimation();
                            if (mMediaPlayer.isPlaying()) {
                                mMediaPlayer.pause();
                            }
                        }
                    } else {
                        for (int i = 0; i < trackingValues.size(); i++) {
                            final TrackingValues tv = trackingValues.get(i);
                            if (tv.isTrackingState()) {
                                mMetaioPlane.setCoordinateSystemID(tv.getCoordinateSystemID());
                                break;
                            }
                        }

                        if (!isAnimation) {
                            mMetaioPlane.startMovieTexture(true);
                        } else {

                            if (mHotspot.getId() == 1 || mHotspot.getId() == 2 || mHotspot.getId() == 6) {
                                mMetaioPlane.startAnimation("Take 001");
                            } else if (mHotspot.getId() == 5) {
                                mMetaioPlane.startAnimation("fp_ENG2");
                            }

                            if (isAudioReady == true) {
                                mMediaPlayer.start();
                            }
                        }
                    }
                }
            }
        }

    }

}
