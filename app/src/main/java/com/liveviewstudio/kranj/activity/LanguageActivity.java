package com.liveviewstudio.kranj.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;

import com.liveviewstudio.kranj.R;
import com.liveviewstudio.kranj.util.Preferences;

import java.util.Locale;


public class LanguageActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        //Buttons
        Button buttonLanguageEnglish = (Button)findViewById(R.id.btn_language_english);
        Button buttonLanguageSlovenian = (Button)findViewById(R.id.btn_language_slovenian);

        //Click listeners
        buttonLanguageEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveSelectedLanguage(getResources().getString(R.string.language_english));
                openInstructionsActivity();
            }
        });
        buttonLanguageSlovenian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveSelectedLanguage(getResources().getString(R.string.language_slovenian));
                openInstructionsActivity();
            }
        });
    }

    /** Saves the selected language to Shared Preferences.
     * @param language Language code. Possible values can be found in {@link R.string}. */
    private void saveSelectedLanguage(final String language) {
        new AsyncTask<String, Void, Void>() {
            @Override
            protected Void doInBackground(String... params) {
                // Update preferences
                Preferences.getInstance().
                        setPreference(Preferences.PreferenceType.LANGUAGE, params[0]);

                // Set new locale
                Locale newLocale = new Locale(params[0]);
                Resources res = getResources();
                DisplayMetrics dm = res.getDisplayMetrics();
                Configuration conf = res.getConfiguration();
                conf.locale = newLocale;
                res.updateConfiguration(conf, dm);

                return null;
            }
        }.execute(language);
    }

    /** Opens instructions activity. New activity slides from right to left. */
    private void openInstructionsActivity() {
        Intent intent = new Intent(LanguageActivity.this, InstructionsActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

}
