package com.liveviewstudio.kranj.activity;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.liveviewstudio.kranj.R;
import com.liveviewstudio.kranj.model.Hotspot;
import com.liveviewstudio.kranj.util.GeneralUtil;

public class DetailsActivity extends Activity {

    private Hotspot mHotspot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Bundle extras = getIntent().getExtras();
        if (extras!=null) {
            mHotspot = extras.getParcelable(getResources().getString(R.string.bundleKeyHotspot));
        }

        if (mHotspot!=null) {
            TextView textDetails = (TextView)findViewById(R.id.text_details);
            ImageView imageDetails = (ImageView)findViewById(R.id.image_details);
            TextView titleDetails = (TextView)findViewById(R.id.title_details);

            String packageName = getPackageName();

            //Title
            int resId = getResources().getIdentifier(mHotspot.getName(), "string", packageName);
            if (resId!=0) {
                titleDetails.setText(getString(resId));
            }

            //Text
            resId = getResources().getIdentifier(mHotspot.getText(), "string", packageName);
            if (resId!=0) {
                textDetails.setText(getString(resId));
            }

            //Image
            String imageName = mHotspot.getDetailsImageName();
            Drawable image = GeneralUtil.getDrawable(DetailsActivity.this, imageName);
            if (image!=null) {
                imageDetails.setImageDrawable(image);
            }
        }
    }

}
