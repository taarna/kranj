package com.liveviewstudio.kranj.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.liveviewstudio.kranj.R;
import com.viewpagerindicator.IconPagerAdapter;

import java.util.ArrayList;


public class InstructionsPagerAdapter extends PagerAdapter implements IconPagerAdapter {

    private Context mContext;
    private ArrayList<Integer> mItemImages;

    protected static final int[] ICONS = new int[] {
            R.drawable.page_indicator_dot,
            R.drawable.page_indicator_dot,
            R.drawable.page_indicator_dot,
            R.drawable.page_indicator_dot
    };

    public InstructionsPagerAdapter(Context context) {
        mContext = context;

        // Images
        TypedArray typedArrayImages = context.getResources().obtainTypedArray(R.array.instructions_images);
        for(int i=0; i<typedArrayImages.length(); i++) {
            if (i==0) {
                mItemImages = new ArrayList<Integer>();
            }
            mItemImages.add(typedArrayImages.getResourceId(i, 0));
        }
        typedArrayImages.recycle();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        imageView.setPadding(0, 0, 0, 0);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView.setImageResource(mItemImages.get(position));

        container.addView(imageView, 0);

        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }

    @Override
    public int getIconResId(int index) {
        return ICONS[index];
    }

    @Override
    public int getCount() {
        return mItemImages.size();
    }

}
