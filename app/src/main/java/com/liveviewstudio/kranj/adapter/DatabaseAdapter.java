package com.liveviewstudio.kranj.adapter;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.liveviewstudio.kranj.helper.DatabaseHelper;
import com.liveviewstudio.kranj.model.Hotspot;
import com.liveviewstudio.kranj.model.Poi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DatabaseAdapter
{
    private static final String TABLE_HOTSPOT = "hotspot";
    private static final String TABLE_POI = "poi";

    private static final String KEY_ID = "_id";
    private static final String KEY_NAME = "name";
    private static final String KEY_TEXT = "text";
    private static final String KEY_PIN_IMAGE = "pin_image";
    private static final String KEY_DETAILS_IMAGE = "details_image";
    private static final String KEY_INFO_IMAGE = "info_window_image";
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";
    private static final String KEY_TRACKING_NAME = "tracking_name";
    private static final String KEY_MODEL_NAME = "model_name";
    private static final String KEY_AUDIO_FILE_NAME = "audio_file_name";

    private static final String[] PROJECTION_HOTSPOT = {
            KEY_ID + ", " +
            KEY_NAME + ", " +
            KEY_TEXT + ", " +
            KEY_PIN_IMAGE + ", " +
            KEY_DETAILS_IMAGE + ", " +
            KEY_INFO_IMAGE + ", " +
            KEY_LATITUDE + ", " +
            KEY_LONGITUDE + ", " +
            KEY_TRACKING_NAME + ", " +
            KEY_MODEL_NAME + ", " +
            KEY_AUDIO_FILE_NAME
    };

    private static final String[] PROJECTION_POI = {
            KEY_ID + ", " +
            KEY_NAME + ", " +
            KEY_LATITUDE + ", " +
            KEY_LONGITUDE
    };

    private static final String SELECTION_POI = KEY_ID + " =? ";

    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;

    public DatabaseAdapter(Context context) {
        mDbHelper = new DatabaseHelper(context);
    }

    public DatabaseAdapter createDatabase() throws SQLException {
        try {
            mDbHelper.createDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    public DatabaseAdapter open() throws SQLException {
        try {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public List<Hotspot> getHotspotList() {
        Cursor cursor = null;
        List<Hotspot> list;

        try {
            cursor = query(TABLE_HOTSPOT, PROJECTION_HOTSPOT, null, null, null, null, null);

            if (cursor != null && cursor.getCount()>0) {
                cursor.moveToFirst();

                list = new ArrayList<>();

                do {

                    //Create hotspot
                    Hotspot hotspot = new Hotspot();
                    hotspot.setId(cursor.getInt(0));
                    hotspot.setName(cursor.getString(1));
                    hotspot.setText(cursor.getString(2));
                    hotspot.setPinImageName(cursor.getString(3));
                    hotspot.setDetailsImageName(cursor.getString(4));
                    hotspot.setInfoImageName(cursor.getString(5));
                    hotspot.setLatitude(cursor.getDouble(6));
                    hotspot.setLongitude(cursor.getDouble(7));
                    hotspot.setTrackingName(cursor.getString(8));
                    hotspot.setModelName(cursor.getString(9));
                    hotspot.setAudioFileName(cursor.getString(10));

                    //Add it to list
                    list.add(hotspot);

                } while (cursor.moveToNext());

            } else {
                return null;
            }

            return list;

        } finally {

            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public List<Poi> getPoiListForHotspot(int hotspotId) {
        Cursor cursor = null;
        List<Poi> list;

        String[] args = {String.valueOf(hotspotId)};

        try {
            cursor = query(TABLE_POI, PROJECTION_POI, SELECTION_POI, args, null, null, null);

            if (cursor != null && cursor.getCount()>0) {
                cursor.moveToFirst();

                list = new ArrayList<>();

                do {

                    //Create hotspot
                    Poi poi = new Poi();
                    poi.setId(cursor.getInt(0));
                    poi.setImageName(cursor.getString(1));
                    poi.setLatitude(cursor.getDouble(2));
                    poi.setLongitude(cursor.getDouble(3));

                    //Add it to list
                    list.add(poi);

                } while (cursor.moveToNext());

            } else {
                return null;
            }

            return list;

        } finally {

            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private Cursor query(String table,
                        String[] projection,
                        String selection,
                        String[] selectionArgs,
                        String groupBy,
                        String having,
                        String orderBy) {
        return mDb.query(table, projection, selection, selectionArgs, groupBy, having, orderBy);
    }

}
