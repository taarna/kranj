package com.liveviewstudio.kranj.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.liveviewstudio.kranj.R;

public class CustomDialog extends Dialog implements android.view.View.OnClickListener {

    private Activity mActivity;

    private String mLeftButtonText, mRightButtonText, mTitle, mText;

    private OnCustomDialogClickListener mcustomDialogListener;


    public interface OnCustomDialogClickListener {
        void onRightClick();
    }
    public void setOnCustomDialogClickListener(OnCustomDialogClickListener listener) {
        this.mcustomDialogListener = listener;
    }

    public CustomDialog(Activity activity) {
        super(activity);

        mActivity = activity;
    }

    public CustomDialog(Activity activity, String leftButtonText, String rightButtonText,
                        String title, String text) {
        super(activity);

        mActivity = activity;

        mLeftButtonText = leftButtonText;
        mRightButtonText = rightButtonText;
        mTitle = title;
        mText = text;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.layout_dialog);

        Button btnLeft = (Button)findViewById(R.id.btn_dialog_left);
        Button btnRight = (Button)findViewById(R.id.btn_dialog_right);

        btnLeft.setText(mLeftButtonText);
        btnRight.setText(mRightButtonText);

        btnLeft.setOnClickListener(this);
        btnRight.setOnClickListener(this);

        TextView title = (TextView)findViewById(R.id.dialog_title_text);
        title.setText(mTitle);

        TextView text = (TextView)findViewById(R.id.dialog_text);
        text.setText(mText);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_dialog_left:
                dismiss();
                break;
            case R.id.btn_dialog_right:
                mcustomDialogListener.onRightClick();
                dismiss();
                break;
            default:
                break;
        }
    }

}
