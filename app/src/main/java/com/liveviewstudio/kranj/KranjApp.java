package com.liveviewstudio.kranj;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import com.liveviewstudio.kranj.util.Preferences;

import java.util.Locale;

public class KranjApp extends Application {

    private Locale locale = null;

    @Override
    public void onCreate() {
        super.onCreate();

        Preferences.init(getApplicationContext().
                getSharedPreferences(
                        getResources().getString(R.string.preferencesName), Context.MODE_PRIVATE));

        Configuration config = getBaseContext().getResources().getConfiguration();
        String lang = Preferences.getInstance().getPreference(Preferences.PreferenceType.LANGUAGE);

        if (!"".equals(lang) && (lang != null) && !config.locale.getLanguage().equals(lang)) {
            locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().
                    updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            getBaseContext().getResources().
                    updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

}
