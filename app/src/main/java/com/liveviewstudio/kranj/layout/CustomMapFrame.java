package com.liveviewstudio.kranj.layout;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.FrameLayout;


public class CustomMapFrame extends FrameLayout {

    private GestureDetector mGestureDetector;
    private OnMapDragListener mDragListener;

    public CustomMapFrame(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        mGestureDetector = new GestureDetector(context, new GestureListener());
    }

    public interface OnMapDragListener {
        void onDrag();
    }

    public void setOnDragListener(OnMapDragListener listener) {
        this.mDragListener = listener;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        mGestureDetector.onTouchEvent(ev);

        return false;
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            return false;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            return false;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            //that's when user starts dragging
            if (mDragListener != null) {
                mDragListener.onDrag();
            }
            return false;
        }
    }

}
