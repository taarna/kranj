package com.liveviewstudio.kranj.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Hotspot implements Parcelable {

    private int mId;
    private String mName;
    private String mText;
    private String mPinImage;
    private String mDetailsImage;
    private String mInfoImage;
    private double mLatitude;
    private double mLongitude;
    private String mTrackingName;
    private String mModelName;
    private String mAudioFileName;

    // constructors
    public Hotspot() {}

    // setters
    public void setId(int id) {
        mId = id;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setText(String text) {
        mText = text;
    }

    public void setPinImageName(String imageName) {
        mPinImage = imageName;
    }

    public void setDetailsImageName(String imageName) {
        mDetailsImage = imageName;
    }

    public void setInfoImageName(String imageName) {
        mInfoImage = imageName;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public void setTrackingName(String trackingName) {
        mTrackingName = trackingName;
    }

    public void setModelName(String modelName) {
        mModelName = modelName;
    }

    public void setAudioFileName(String audioFileName) {
        mAudioFileName = audioFileName;
    }

    // getters
    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getText() {
        return mText;
    }

    public String getPinImageName() {
        return mPinImage;
    }

    public String getDetailsImageName() {
        return mDetailsImage;
    }

    public String getInfoImageName() {
        return mInfoImage;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public String getTrackingName() {
        return mTrackingName;
    }

    public String getModelName() {
        return mModelName;
    }

    public String getAudioFileName() {
        return mAudioFileName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mId);
        parcel.writeString(mName);
        parcel.writeString(mText);
        parcel.writeString(mPinImage);
        parcel.writeString(mDetailsImage);
        parcel.writeString(mInfoImage);
        parcel.writeDouble(mLatitude);
        parcel.writeDouble(mLongitude);
        parcel.writeString(mTrackingName);
        parcel.writeString(mModelName);
        parcel.writeString(mAudioFileName);
    }

    public static final Parcelable.Creator<Hotspot> CREATOR
            = new Parcelable.Creator<Hotspot>() {
        public Hotspot createFromParcel(Parcel in) {
            return new Hotspot(in);
        }

        public Hotspot[] newArray(int size) {
            return new Hotspot[size];
        }
    };

    private Hotspot(Parcel in) {
        mId = in.readInt();
        mName = in.readString();
        mText = in.readString();
        mPinImage = in.readString();
        mDetailsImage = in.readString();
        mInfoImage = in.readString();
        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
        mTrackingName = in.readString();
        mModelName = in.readString();
        mAudioFileName = in.readString();
    }

    public void clear() {
        setName(null);
        setText(null);
        setPinImageName(null);
        setDetailsImageName(null);
        setInfoImageName(null);
        setTrackingName(null);
        setModelName(null);
        setAudioFileName(null);
    }

}
