package com.liveviewstudio.kranj.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Poi implements Parcelable {

    private int mId;
    private String mName;
    private double mLatitude;
    private double mLongitude;
    private String mImage;

    // constructors
    public Poi() {}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mName);
        dest.writeDouble(mLatitude);
        dest.writeDouble(mLongitude);
        dest.writeString(mImage);
    }

    public static final Parcelable.Creator<Poi> CREATOR
            = new Parcelable.Creator<Poi>() {
        public Poi createFromParcel(Parcel in) {
            return new Poi(in);
        }

        public Poi[] newArray(int size) {
            return new Poi[size];
        }
    };

    private Poi(Parcel in) {
        mId = in.readInt();
        mName = in.readString();
        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
        mImage = in.readString();
    }

    public void setId(int id) {
        mId = id;
    }
    public int getId() {
        return mId;
    }

    public void setName(String name) {
        mName = name;
    }
    public String getName() {
        return mName;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }
    public double getLatitude() {
        return mLatitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }
    public double getLongitude() {
        return mLongitude;
    }

    public void setImageName(String imageName) {
        mImage = imageName;
    }
    public String getImageName() {
        return mImage;
    }

}
